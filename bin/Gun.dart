import 'Shoot.dart';
import 'Weapon.dart';

class Gun extends Weapon implements Shoot{
  Gun() : super('Gun');

  @override
  void shooting() {
    print("Gun Shooting!!!");
  }

}
import 'dart:ffi';

import 'package:test/expect.dart';

import 'TableMap.dart';
import 'Weapon.dart';

class Player {
  TableMap? tableMap;
  Weapon? weapon;
  var x;
  var y;
  var symbol;
  var hp;
  var zombie;

  Player(int x, int y, String symbol, TableMap tableMap, int hp,int zombie) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    this.tableMap = tableMap;
    this.hp = hp;
    this.zombie = zombie;
  }
  getX() {
    return x;
  }

  getY() {
    return y;
  }

  getSymbol() {
    return symbol;
  }

  void reduceHp() {
    hp--;
  }

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (walkN()) return false;
        break;
      case 'S':
      case 's':
        if (walkS()) return false;
        break;
      case 'E':
      case 'd':
        if (walkE()) return false;

        break;
      case 'W':
      case 'a':
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    return true;
  }

  bool canWalk(int x, int y) {
    return hp > 0 && tableMap!.Inmap(x, y) && tableMap!.isWall(x, y) == false;
  }

  bool check() {
    int hp = tableMap!.fillHP(x, y);
    while (hp > 0) {
      zombie--;
      this.hp += hp;
      return true;
    }
    
    return false;
  }
  bool checkHP(){
    if(hp == 0){
      return true;
    }
    return false;
  }
  bool checkZombie(){
    if(zombie == 0){
      return true;
    }
    return false;
  }

  bool walkW() {
    check();
    checkHP();
    checkZombie();
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    reduceHp();
    return false;
  }

  bool walkE() {
    check();
    checkHP();
    checkZombie();
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    reduceHp();
    return false;
  }

  bool walkS() {
    check();
    checkHP();
    checkZombie();
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    reduceHp();
    return false;
  }

  bool walkN() {
    check();
    checkHP();
    checkZombie();
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    reduceHp();
    return false;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  toString() {
    return "Player >>> HP: " +hp.toString() + "  Zombie Amount: >>> " + zombie.toString() +" <<<\n" +"===================================";
  }
}

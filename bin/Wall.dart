import 'dart:ffi';

import 'TableMap.dart';

class Wall {
  var x;
  var y;
  var symbol;

 Wall(int x, int y , String symbol) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  getX() {
    return x;
  }

  getY() {
    return y;
  }

  getSymbol() {
    return symbol;
  }
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}

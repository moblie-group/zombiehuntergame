import 'dart:ffi';
import "dart:io";

import 'Player.dart';
import 'Wall.dart';
import 'Zombie.dart';

class TableMap {
  var height;
  var width;
  Player? player;
  // TableMap? tableMap;
  List<Wall> wall = [];
  // List playerl = List.filled(100, Player);
  List<Player> playerl = [];
  List<Zombie> zombie = [];
  var wallCount = 0;
  var PlayerCount = 0;
  var zombieCount = 0;

  TableMap(int height, int width) {
    this.height = height;
    this.width = width;
  }

  SymbolObject(int x, int y) {
    String symbol = '⠀⠀';
    for (int o = 0; o < wallCount; o++) {
      if (wall[o].isOn(x, y)) {
        symbol = wall[o].getSymbol();
      }
    }
    for (int o = 0; o < zombieCount; o++) {
      if (zombie[o].isOn(x, y)) {
        symbol = zombie[o].getSymbol();
      }
    }
    for (int o = 0; o < PlayerCount; o++) {
      if (playerl[o].isOn(x, y)) {
        symbol = playerl[o].getSymbol();
      }
    }

    stdout.write(symbol);
  }

  Showmap() {
    Title();
    print(player);
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        // if (player!.isOn(x, y)) {
        //   ShowPlayer();
        // } else {
        //   ShowLine();
        // }
        SymbolObject(x, y);
      }
      Newline();
    }
  }

  Title() {
    print("Map");
  }

  Newline() {
    print("");
  }

  ShowPlayer() {
    stdout.write(player!.getSymbol());
  }

  // ShowLine() {
  //   stdout.write('-');
  // }

  Inmap(int x, int y) {
    return x < width && x >= 0 && y >= 0 && y < height;
  }
  // InO(int x, int y){
  //   return tableMap!.Inmap(x, y)&& !tableMap!.isWall(x, y);
  // }

  setPlayer(Player player) {
    this.player = player;
    AddPlayer(player);
  }

  AddPlayer(Player p) {
    // playerl[PlayerCount] = p;
    // PlayerCount++;
    playerl.add(p);
    PlayerCount++;
  }

  AddWall(Wall w) {
    wall.add(w);
    wallCount++;
  }

  AddZombie(Zombie z) {
    zombie.add(z);
    zombieCount++;
  }

  bool isWall(int x, int y) {
    for (int o = 0; o < wallCount; o++) {
      if (wall[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  fillHP(int x, int y) {
    for (int o = 0; o < zombieCount; o++) {
      if (zombie[o].isOn(x, y)) {
        Zombie zombies = zombie[o];
        return zombies.fillHP();
      }
    }
    return 0;
  }
  
}

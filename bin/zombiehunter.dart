import 'dart:io';
import 'dart:ffi';
import 'Bow.dart';
import 'Gun.dart';
import 'Knife.dart';
import 'Player.dart';
import 'Sword.dart';
import 'TableMap.dart';
import 'Wall.dart';
import 'Weapon.dart';
import 'Zombie.dart';

class zombiehunter {
  void showWelcome() {
    print("\x1B[2J\x1B[0;0H");
    print(
        """▒███████▒ ▒█████   ███▄ ▄███▓ ▄▄▄▄    ██▓▓█████     ██░ ██  █    ██  ███▄    █ ▄▄▄█████▓▓█████  ██▀███  
▒ ▒ ▒ ▄▀░▒██▒  ██▒▓██▒▀█▀ ██▒▓█████▄ ▓██▒▓█   ▀    ▓██░ ██▒ ██  ▓██▒ ██ ▀█   █ ▓  ██▒ ▓▒▓█   ▀ ▓██ ▒ ██▒
░ ▒ ▄▀▒░ ▒██░  ██▒▓██    ▓██░▒██▒ ▄██▒██▒▒███      ▒██▀▀██░▓██  ▒██░▓██  ▀█ ██▒▒ ▓██░ ▒░▒███   ▓██ ░▄█ ▒
  ▄▀▒   ░▒██   ██░▒██    ▒██ ▒██░█▀  ░██░▒▓█  ▄    ░▓█ ░██ ▓▓█  ░██░▓██▒  ▐▌██▒░ ▓██▓ ░ ▒▓█  ▄ ▒██▀▀█▄  
▒███████▒░ ████▓▒░▒██▒   ░██▒░▓█  ▀█▓░██░░▒████▒   ░▓█▒░██▓▒▒█████▓ ▒██░   ▓██░  ▒██▒ ░ ░▒████▒░██▓ ▒██▒
░▒▒ ▓░▒░▒░ ▒░▒░▒░ ░ ▒░   ░  ░░▒▓███▀▒░▓  ░░ ▒░ ░    ▒ ░░▒░▒░▒▓▒ ▒ ▒ ░ ▒░   ▒ ▒   ▒ ░░   ░░ ▒░ ░░ ▒▓ ░▒▓░
░░▒ ▒ ░ ▒  ░ ▒ ▒░ ░  ░      ░▒░▒   ░  ▒ ░ ░ ░  ░    ▒ ░▒░ ░░░▒░ ░ ░ ░ ░░   ░ ▒░    ░     ░ ░  ░  ░▒ ░ ▒░
░ ░ ░ ░ ░░ ░ ░ ▒  ░      ░    ░    ░  ▒ ░   ░       ░  ░░ ░ ░░░ ░ ░    ░   ░ ░   ░         ░     ░░   ░ 
  ░ ░        ░ ░         ░    ░       ░     ░  ░    ░  ░  ░   ░              ░             ░  ░   ░     
░                                  ░                                                                    """);
  }

  void mainMenu() {
    showWelcome();
    stdout.write((':: 1 ::'));
    print(('PLAY'));
    stdout.write((':: 2 ::'));
    print(('EXIT'));
  }

  bool enterGame() {
    int select = 0;

    do {
      String? choice = stdin.readLineSync();
      if (choice == '1') {
        return true;
      } else if (choice == '2') {
        return false;
      } else {
        print("\x1B[2J\x1B[0;0H");
        mainMenu();
      }
    } while (select == 0);

    return false;
  }

  void showType() {
    print("\x1B[2J\x1B[0;0H");
    print("1.Bow");
    print("2.Gun");
    print("3.Sword");
    print("4.Knife");
  }

  void selectType() {
    String select = (stdin.readLineSync()!);
    if (select == "1") {
      Bow().shooting();
      print("Press the Enter key to continue.");
    } else if (select == "2") {
      Gun().shooting();
      print("Press the Enter key to continue.");
    } else if (select == "3") {
      Sword().Slashshing();
      print("Press the Enter key to continue.");
    } else if (select == "4") {
      Knife().Slashshing();
      print("Press the Enter key to continue.");
    } else {}
  }
}

void main(List<String> arguments) {
  TableMap tableMap = TableMap(11, 15);
  Player? player = Player(2, 0, '🚶', tableMap, 10, 4);
  zombiehunter? zombiegame = zombiehunter();

  tableMap.AddZombie(Zombie(5, 6, "🧟", 10));
  tableMap.AddZombie(Zombie(5, 9, "🧟", 10));
  tableMap.AddZombie(Zombie(8, 4, "🧟", 10));
  tableMap.AddZombie(Zombie(13, 1, "🧟", 10));
  tableMap.AddWall(Wall(0, 0, "🧱"));
  tableMap.AddWall(Wall(0, 1, "🧱"));
  tableMap.AddWall(Wall(0, 2, "🧱"));
  tableMap.AddWall(Wall(0, 3, "🧱"));
  tableMap.AddWall(Wall(0, 4, "🧱"));
  tableMap.AddWall(Wall(0, 5, "🧱"));
  tableMap.AddWall(Wall(0, 6, "🧱"));
  tableMap.AddWall(Wall(0, 7, "🧱"));
  tableMap.AddWall(Wall(0, 8, "🧱"));
  tableMap.AddWall(Wall(0, 9, "🧱"));
  tableMap.AddWall(Wall(0, 10, "🧱"));
  tableMap.AddWall(Wall(0, 11, "🧱"));
  tableMap.AddWall(Wall(1, 10, "🧱"));
  tableMap.AddWall(Wall(2, 10, "🧱"));
  tableMap.AddWall(Wall(3, 10, "🧱"));
  tableMap.AddWall(Wall(4, 10, "🧱"));
  tableMap.AddWall(Wall(5, 10, "🧱"));
  tableMap.AddWall(Wall(6, 10, "🧱"));
  tableMap.AddWall(Wall(7, 10, "🧱"));
  tableMap.AddWall(Wall(8, 10, "🧱"));
  tableMap.AddWall(Wall(9, 10, "🧱"));
  tableMap.AddWall(Wall(10, 10, "🧱"));
  tableMap.AddWall(Wall(11, 10, "🧱"));
  tableMap.AddWall(Wall(12, 10, "🧱"));
  tableMap.AddWall(Wall(13, 10, "🧱"));
  tableMap.AddWall(Wall(14, 10, "🧱"));
  tableMap.AddWall(Wall(19, 0, "🧱"));
  tableMap.AddWall(Wall(18, 0, "🧱"));
  tableMap.AddWall(Wall(17, 0, "🧱"));
  tableMap.AddWall(Wall(16, 0, "🧱"));
  tableMap.AddWall(Wall(15, 0, "🧱"));
  tableMap.AddWall(Wall(14, 0, "🧱"));
  tableMap.AddWall(Wall(13, 0, "🧱"));
  tableMap.AddWall(Wall(12, 0, "🧱"));
  tableMap.AddWall(Wall(11, 0, "🧱"));
  tableMap.AddWall(Wall(10, 0, "🧱"));
  tableMap.AddWall(Wall(14, 1, "🧱"));
  tableMap.AddWall(Wall(14, 2, "🧱"));
  tableMap.AddWall(Wall(14, 3, "🧱"));
  tableMap.AddWall(Wall(14, 4, "🧱"));
  tableMap.AddWall(Wall(14, 5, "🧱"));
  tableMap.AddWall(Wall(14, 6, "🧱"));
  tableMap.AddWall(Wall(14, 7, "🧱"));
  tableMap.AddWall(Wall(14, 8, "🧱"));
  tableMap.AddWall(Wall(14, 9, "🧱"));
  tableMap.AddWall(Wall(14, 10, "🧱"));
  tableMap.AddWall(Wall(9, 0, "🧱"));
  tableMap.AddWall(Wall(8, 0, "🧱"));
  tableMap.AddWall(Wall(7, 0, "🧱"));
  tableMap.AddWall(Wall(6, 0, "🧱"));
  tableMap.AddWall(Wall(5, 0, "🧱"));
  tableMap.AddWall(Wall(4, 0, "🧱"));
  tableMap.AddWall(Wall(3, 0, "🧱"));
  tableMap.AddWall(Wall(1, 0, "🧱"));
  tableMap.AddWall(Wall(2, 2, "🧱"));
  tableMap.AddWall(Wall(2, 3, "🧱"));
  tableMap.AddWall(Wall(2, 4, "🧱"));
  tableMap.AddWall(Wall(2, 5, "🧱"));
  tableMap.AddWall(Wall(4, 5, "🧱"));
  tableMap.AddWall(Wall(5, 5, "🧱"));
  tableMap.AddWall(Wall(5, 7, "🧱"));
  tableMap.AddWall(Wall(4, 7, "🧱"));
  tableMap.AddWall(Wall(2, 7, "🧱"));
  tableMap.AddWall(Wall(4, 1, "🧱"));
  tableMap.AddWall(Wall(4, 2, "🧱"));
  tableMap.AddWall(Wall(4, 3, "🧱"));
  tableMap.AddWall(Wall(4, 4, "🧱"));
  tableMap.AddWall(Wall(2, 9, "🧱"));
  tableMap.AddWall(Wall(2, 8, "🧱"));
  tableMap.AddWall(Wall(12, 9, "🧱"));
  tableMap.AddWall(Wall(12, 8, "🧱"));
  tableMap.AddWall(Wall(12, 7, "🧱"));
  tableMap.AddWall(Wall(12, 5, "🧱"));
  tableMap.AddWall(Wall(6, 7, "🧱"));
  tableMap.AddWall(Wall(7, 7, "🧱"));
  tableMap.AddWall(Wall(8, 7, "🧱"));
  tableMap.AddWall(Wall(9, 7, "🧱"));
  tableMap.AddWall(Wall(10, 7, "🧱"));
  tableMap.AddWall(Wall(6, 5, "🧱"));
  tableMap.AddWall(Wall(7, 5, "🧱"));
  tableMap.AddWall(Wall(8, 5, "🧱"));
  tableMap.AddWall(Wall(9, 5, "🧱"));
  tableMap.AddWall(Wall(10, 5, "🧱"));
  tableMap.AddWall(Wall(12, 3, "🧱"));
  tableMap.AddWall(Wall(12, 2, "🧱"));
  tableMap.AddWall(Wall(11, 3, "🧱"));
  tableMap.AddWall(Wall(10, 3, "🧱"));
  tableMap.AddWall(Wall(9, 3, "🧱"));
  tableMap.AddWall(Wall(8, 3, "🧱"));
  tableMap.AddWall(Wall(7, 3, "🧱"));
  tableMap.AddWall(Wall(6, 3, "🧱"));
  tableMap.AddWall(Wall(6, 2, "🧱"));
  tableMap.AddWall(Wall(7, 2, "🧱"));
  tableMap.AddWall(Wall(8, 2, "🧱"));
  tableMap.AddWall(Wall(9, 2, "🧱"));
  tableMap.AddWall(Wall(10, 2, "🧱"));
  tableMap.AddWall(Wall(11, 2, "🧱"));
  tableMap.AddWall(Wall(4, 8, "🧱"));
  tableMap.AddWall(Wall(5, 8, "🧱"));
  tableMap.AddWall(Wall(6, 8, "🧱"));
  tableMap.AddWall(Wall(7, 8, "🧱"));
  tableMap.AddWall(Wall(8, 8, "🧱"));
  tableMap.AddWall(Wall(9, 8, "🧱"));
  tableMap.AddWall(Wall(10, 8, "🧱"));
  tableMap.setPlayer(player);
  print(tableMap.wallCount);
  zombiegame.mainMenu();

  if (zombiegame.enterGame() == true) {
    while (true) {
      print("\x1B[2J\x1B[0;0H");
      print((''' 
    " how to play "
>>> 1 walk =>>  w is ^ || a is < || s  is v || d is >
>>> 2 For each walk, hp decreases by 1.
>>> 3 If hp = 0 the game will be over.
>>> 4 If Zombie is left 0, you pass the game.
>>> 5 If you kill Zombie hp you will add 10.
>>> 6 Press q to exit the game.
    
    '''));
      tableMap.Showmap();
      if (player.check() == true) {
        zombiegame.showType();
        zombiegame.selectType();
      }
      if (player.checkHP() == true) {
        gameover();
      }

      if (player.checkZombie() == true) {
        congratulations();
      }
      String direction = Input();
      player.walk(direction);
      if (direction == 'q') {
        Goodbye();
        break;
      }
    }
  } else {
    Goodbye();
  }
}

Goodbye() {
  print("\x1B[2J\x1B[0;0H");
  print("""
  ▄████  ▒█████   ▒█████  ▓█████▄  ▄▄▄▄   ▓██   ██▓▓█████ 
 ██▒ ▀█▒▒██▒  ██▒▒██▒  ██▒▒██▀ ██▌▓█████▄  ▒██  ██▒▓█   ▀ 
▒██░▄▄▄░▒██░  ██▒▒██░  ██▒░██   █▌▒██▒ ▄██  ▒██ ██░▒███   
░▓█  ██▓▒██   ██░▒██   ██░░▓█▄   ▌▒██░█▀    ░ ▐██▓░▒▓█  ▄ 
░▒▓███▀▒░ ████▓▒░░ ████▓▒░░▒████▓ ░▓█  ▀█▓  ░ ██▒▓░░▒████▒
 ░▒   ▒ ░ ▒░▒░▒░ ░ ▒░▒░▒░  ▒▒▓  ▒ ░▒▓███▀▒   ██▒▒▒ ░░ ▒░ ░
  ░   ░   ░ ▒ ▒░   ░ ▒ ▒░  ░ ▒  ▒ ▒░▒   ░  ▓██ ░▒░  ░ ░  ░
░ ░   ░ ░ ░ ░ ▒  ░ ░ ░ ▒   ░ ░  ░  ░    ░  ▒ ▒ ░░     ░   
      ░     ░ ░      ░ ░     ░     ░       ░ ░        ░  ░
                           ░            ░  ░ ░            
""");
}

congratulations() {
  print("\x1B[2J\x1B[0;0H");
  print("""
 ▄████████  ▄██████▄  ███▄▄▄▄      ▄██████▄     ▄████████    ▄████████     ███     ███    █▄   ▄█          ▄████████     ███      ▄█   ▄██████▄  ███▄▄▄▄      ▄████████ 
███    ███ ███    ███ ███▀▀▀██▄   ███    ███   ███    ███   ███    ███ ▀█████████▄ ███    ███ ███         ███    ███ ▀█████████▄ ███  ███    ███ ███▀▀▀██▄   ███    ███ 
███    █▀  ███    ███ ███   ███   ███    █▀    ███    ███   ███    ███    ▀███▀▀██ ███    ███ ███         ███    ███    ▀███▀▀██ ███▌ ███    ███ ███   ███   ███    █▀  
███        ███    ███ ███   ███  ▄███         ▄███▄▄▄▄██▀   ███    ███     ███   ▀ ███    ███ ███         ███    ███     ███   ▀ ███▌ ███    ███ ███   ███   ███        
███        ███    ███ ███   ███ ▀▀███ ████▄  ▀▀███▀▀▀▀▀   ▀███████████     ███     ███    ███ ███       ▀███████████     ███     ███▌ ███    ███ ███   ███ ▀███████████ 
███    █▄  ███    ███ ███   ███   ███    ███ ▀███████████   ███    ███     ███     ███    ███ ███         ███    ███     ███     ███  ███    ███ ███   ███          ███ 
███    ███ ███    ███ ███   ███   ███    ███   ███    ███   ███    ███     ███     ███    ███ ███▌    ▄   ███    ███     ███     ███  ███    ███ ███   ███    ▄█    ███ 
████████▀   ▀██████▀   ▀█   █▀    ████████▀    ███    ███   ███    █▀     ▄████▀   ████████▀  █████▄▄██   ███    █▀     ▄████▀   █▀    ▀██████▀   ▀█   █▀   ▄████████▀  
                                               ███    ███                                     ▀                                                                         
""");
}

gameover() {
  print("\x1B[2J\x1B[0;0H");
  print("""
  ▄▀  ██   █▀▄▀█ ▄███▄       ████▄     ▄   ▄███▄   █▄▄▄▄          
▄▀    █ █  █ █ █ █▀   ▀      █   █      █  █▀   ▀  █  ▄▀          
█ ▀▄  █▄▄█ █ ▄ █ ██▄▄        █   █ █     █ ██▄▄    █▀▀▌           
█   █ █  █ █   █ █▄   ▄▀     ▀████  █    █ █▄   ▄▀ █  █           
 ███     █    █  ▀███▀               █  █  ▀███▀     █   ██ ██ ██ 
        █    ▀                        █▐            ▀             
       ▀                              ▐                           
""");
}

Input() {
  String? direction = stdin.readLineSync();
  return direction;
}

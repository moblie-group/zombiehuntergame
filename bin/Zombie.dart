import 'TableMap.dart';

class Zombie {
  TableMap? tableMap;
  var x;
  var y;
  var symbol;
  var volume;

  Zombie(int x, int y, String symbol ,int volume) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    this.volume = volume;
  }
  getX() {
    return x;
  }

  getY() {
    return y;
  }

  getSymbol() {
    return symbol;
  }
  getVolume(){
    return volume;
  }

   fillHP(){
    int vol = volume;
        symbol = '⠀⠀';
        volume = 0 ;
        return vol;
  }
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}
